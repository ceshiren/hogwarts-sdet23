"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import yaml

from litemall.utils.log_util import logger


class LoadDataUtils:
    @classmethod
    def load_data_by_yaml(cls, file_name):
        with open(file_name) as f:
            data = yaml.safe_load(f)
        logger.debug(f"读取的配置文件为{data}")
        return data

    @classmethod
    def load_data_by_json(self):
        return