from selenium.webdriver.common.by import By

from litemall.web.page_objects.base_page import BasePage


class IndexPage(BasePage):
    _MENU_PRODUCT_MANAGE = (By.XPATH, "//span[contains(text(), '商品管理')]")
    _MENU_PRODUCT_GROUNDING = (By.XPATH, "//span[contains(text(), '商品上架')]")

    def go_to_product_grounding(self):
        # 点击 【商品管理 】 菜单
        self.do_click(*self._MENU_PRODUCT_MANAGE)
        # 点击 【商品上架】 菜单
        self.do_click(*self._MENU_PRODUCT_GROUNDING)

        from litemall.web.page_objects.goods_grounding_page import GoodsGroundingPage
        return GoodsGroundingPage(self.driver)