from selenium.webdriver.common.by import By

from litemall.web.page_objects.base_page import BasePage


class GoodsGroundingPage(BasePage):
    _INPUT_GOODS_NUM = (By.XPATH, "//label[@for='goodsSn']/../div/div/input")
    _INPUT_GOODS_NAME = (By.XPATH, "//label[@for='name']/../div/div/input")
    _INPUT_GOODS_PRICE = (By.XPATH, "//label[@for='counterPrice']/../div/div/input")
    _RADIO_HOT = (By.XPATH, '//span[contains(text(), "热卖")]')
    _BNT_GOUNDING = (By.XPATH, "//div[@class='app-container']//span[text()='上架']")

    def goods_grounding(self):
        # 输入【商品编号】
        self.do_send_keys("1111113",*self._INPUT_GOODS_NUM)
        # 输入【商品名称】
        self.do_send_keys("hogwarts1111113",*self._INPUT_GOODS_NAME)
        # 输入【市场售价】
        self.do_send_keys("1233",*self._INPUT_GOODS_PRICE)
        # 选择【热卖】按钮
        self.do_click(*self._RADIO_HOT)
        # 滑动到底部
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")

        # 点击【上架】按钮
        self.do_click(*self._BNT_GOUNDING)

        from litemall.web.page_objects.goods_list_page import GoodsListPage
        return GoodsListPage(self.driver)


