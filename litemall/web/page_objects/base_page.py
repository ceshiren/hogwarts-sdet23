"""
基类:实例化driver ,封装所有的基本方法
"""
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


class BasePage:
    _BASE_URL = "https://litemall.hogwarts.ceshiren.com/"

    def __init__(self, driver: WebDriver = None):
        if driver is None:
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(5)
            self.driver.maximize_window()
            # 打开litemall 网站
            self.driver.get(self._BASE_URL)
        else:
            # 复用driver
            self.driver = driver

    def do_click(self, by: By, locator: str):
        """ 查找元素并点击"""
        self.driver.find_element(by, locator).click()

    def do_send_keys(self, text, by: By, locator: str):
        """查找元素并输入"""
        element = self.driver.find_element(by,locator)
        # 先清空
        element.clear()
        element.send_keys(text)
