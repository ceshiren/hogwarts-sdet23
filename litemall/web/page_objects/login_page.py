from selenium.webdriver.common.by import By

from litemall.web.page_objects.base_page import BasePage



class LoginPage(BasePage):
    _INPUT_USERNAME = (By.XPATH, "//*[@name='username']")
    _INPUT_PASSWORD = (By.XPATH, "//*[@name='password']")
    _BNT_LOGIN = (By.CSS_SELECTOR, "button.el-button")

    def login(self):
        """登录"""
        # 输入 用户名
        self.do_send_keys("admin123", *self._INPUT_USERNAME)
        # 输入 密码
        self.do_send_keys("admin123", *self._INPUT_PASSWORD)
        # 点击 登录
        self.do_click(*self._BNT_LOGIN)
        from litemall.web.page_objects.index_page import IndexPage
        return IndexPage(self.driver)
