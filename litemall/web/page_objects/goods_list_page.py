from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from litemall.web.page_objects.base_page import BasePage


class GoodsListPage(BasePage):
    _TEXT_PRODUCT_NAME = (By.XPATH, "//tbody/tr[1]/td[3]/div")

    def get_product_name(self):
        """获取商品名称"""
        element = WebDriverWait(self.driver,10).until(expected_conditions.visibility_of_element_located(self._TEXT_PRODUCT_NAME))
        name = element.text

        return  name
