from litemall.web.page_objects.login_page import LoginPage


class TestProductManage:
    def test_product_grounding(self):
        self.login = LoginPage()
        name = self.login.login()\
            .go_to_product_grounding()\
            .goods_grounding()\
            .get_product_name()
        assert name == "hogwarts1111113"