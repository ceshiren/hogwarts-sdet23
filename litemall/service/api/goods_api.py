"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import requests

from litemall.service.api.litemall_api import LitemallApi


class GoodsApi(LitemallApi):
    def list(self,  goods_name, order="desc", sort="add_time"):
        # path = "get"
        # method = "get"
        # json = {"goods_name": goods_name}
        goods_list_url = "admin/goods/list"
        goods_data = {
            "name": goods_name,
            "order": order,
            "sort": sort
        }
        r = self.send("get", goods_list_url, params=goods_data,)
        return r

