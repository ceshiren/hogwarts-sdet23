"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import requests

from litemall.utils.log_util import logger


# 1. 把token封装在请求中。
# 2. 封装一个send方法

class LitemallApi:
    def __init__(self, base_url):
        self.base_url = base_url

    def __set_token(self, **kwargs):
        """
        设置token
        """
        # 为了避免token 的重复初始化
        # 通过has自己这个对象本身有没有token属性。去判断是否需要token初始化
        if not hasattr(self, "token"):
            admin_data = {"username":"admin123","password":"admin123","code":""}
            r = requests.request("post","https://litemall.hogwarts.ceshiren.com/admin/auth/login", json=admin_data)
            self.token = {"X-Litemall-Admin-Token": r.json()["data"]["token"]}
        if kwargs.get("headers"):
            kwargs.update(self.token)
        else:
            kwargs["headers"] = self.token
        return kwargs


    def send(self, method, path, **kwargs):
        # 对请求信息做了二次处理
        kwargs = self.__set_token(**kwargs)
        logger.debug(f"请求信息方法：{method}，请求URL:{self.base_url+path}，其他请求信息{kwargs}")
        r = requests.request(method, self.base_url+path,**kwargs)
        # 调试代码时可以通过添加代理，查看请求信息
        logger.debug(f"响应信息为{r.text}")
        return r.json()
