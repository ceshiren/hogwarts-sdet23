"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import os

from litemall.service.api.goods_api import GoodsApi
from litemall.utils.jsonpath_utils import JsonpathUitls
from litemall.utils.load_data_utils import LoadDataUtils


class TestGoodsApi:
    def setup_class(self):
        # 第一种方式
        # export service_env=test
        # pytest test_litemall
        # 第二种方式
        # pytest test_litemall --env=test
        # base_url =
        env = os.getenv("service_env", default="dev")
        env_data = LoadDataUtils.load_data_by_yaml(f"litemall/config/{env}.yaml")
        self.goods = GoodsApi(env_data["base_url"])

    def test_get(self):
        r = self.goods.list("图画本")
        res = JsonpathUitls.get_jsonpath(r, "$..name")
        # print(res)
        assert res[0] == "图画本"