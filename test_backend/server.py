"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
# 初始化
from flask import Flask
from flask_cors import CORS
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import Session


app = Flask(__name__)
CORS(app, supports_credentials=True)
api = Api(app)

username = "root"
password = "123456"
server = "134.175.28.202"
database = "ck23"

app.config['SQLALCHEMY_DATABASE_URI'] = f"mysql+pymysql://{username}:{password}@{server}/{database}?charset=utf8"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# SQLAlchemy 绑定 app
db = SQLAlchemy(app)
db_session: Session = db.session

def add_router():
    from test_backend.controller.build_controller import build_ns
    from test_backend.controller.plan_controller import plan_ns
    from test_backend.controller.testcase_controller import testcase_ns

    api.add_namespace(testcase_ns, "/testcase")
    api.add_namespace(plan_ns, "/plan")
    api.add_namespace(build_ns, "/build")

if __name__ == '__main__':
    add_router()
    app.run(debug=True)

