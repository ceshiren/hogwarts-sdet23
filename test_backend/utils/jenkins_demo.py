"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
"""
@Author: 霍格沃兹测试学社-AD
@Date: 2021/12/21 7:58 下午
"""

# 注意：！！！！ 如果没看录播的同学一定要补充录播
# 获取Jenkins的版本
from jenkinsapi.jenkins import Jenkins
# Jenkins 服务
BASE_URL = "http://www.loseweight.ren:8080/"
# Jenkins 服务对应的用户名
USERNAME = "admin"
# Jenkins 服务对应的token
PASSWORD = "1148fc3fb0ffc0d352b37981390b5bfd8c"
# 获取了一个jenkins示例
jenkins_hogwarts = Jenkins(BASE_URL, USERNAME, PASSWORD)
print(jenkins_hogwarts.version)

# 获取Jenkins 的job 对象
job = jenkins_hogwarts.get_job("hogwarts")
# 构建hogwarts job， 传入的值必须是字典， key 对应 jenkins 设置的参数名
job.invoke(build_params={"task": "hogwarts_Ad"})
# 获取job 最后一次完成构建的编号
print(job.get_last_buildnumber())
