"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
"""
@Author: 霍格沃兹测试学社-AD
@Date: 2021/12/21 7:58 下午
"""

# 注意：！！！！ 如果没看录播的同学一定要补充录播
#报错内容 ：  401 Client Error: Unauthorized for url
# 认证问题
# 获取Jenkins的版本
from jenkinsapi.jenkins import Jenkins
# 目的： 能够执行测试用例
# 实现：
class JenkinsUtils:
    @classmethod
    def invoke(self, testcase_info: str)->str:
        # Jenkins 服务
        BASE_URL = "http://www.loseweight.ren:8080/"
        # Jenkins 服务对应的用户名
        USERNAME = "admin"
        # Jenkins 服务对应的token
        PASSWORD = "11c2e7878d08579a845cc79f613d8b8e95"
        job_name = "ck23"
        # 获取了一个jenkins示例
        jenkins_hogwarts = Jenkins(BASE_URL, USERNAME, PASSWORD)
        # 获取Jenkins 的job 对象
        job = jenkins_hogwarts.get_job(job_name)
        # 构建hogwarts job， 传入的值必须是字典， key 对应 jenkins 设置的参数名
        job.invoke(build_params={"task": testcase_info})
        # 获取job 最后一次完成构建的编号
        # 问题： 如何获取报告
        # 方式一： 比较规范，大项目是使用这种方法， 直接从报告结果提取数据，然后自己组装，缺点：
        # 实现麻烦，费时费力
        # 方式二： 直接把allure报告返回出去。
        # http://www.loseweight.ren:8080/job/ck23/20/allure/
        last_build_number = job.get_last_buildnumber()
        # 问题： last_build_number 获取的永远是最新那次的上一次构建记录
        report = f"{BASE_URL}job/{job_name}/{last_build_number+1}/allure/"
        return report


if __name__ == '__main__':
    print(JenkinsUtils.invoke("test_calc.py"))