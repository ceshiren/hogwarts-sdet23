"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from test_backend.service.build_service import BuildService


class TestBuildService:

    def setup_class(self):
        self.build = BuildService()

    def test_get_all(self):
        print(self.build.get_all())


    def test_get_all_by_plan_id(self):
        res = self.build.get_all_by_plan_id(1)
        print(res)
