"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from test_backend.do.plan_do import PlanDo
from test_backend.service.plan_service import PlanService
from test_backend.service.testcase_service import TestcaseService


class TestPlanService:
    def setup_class(self):
        self.plan_service = PlanService()

    def test_save(self):
        testcase_id_list = [4, 5]
        plan_entity = PlanDo(name="测试计算器")
        self.plan_service.save(testcase_id_list, plan_entity)

    def test_get_all(self):
        print(self.plan_service.get_all())

    def test_execute(self):
        testcase_service = TestcaseService()
        testcases = [testcase_service.get(i) for i in [4, 5]]
        self.plan_service.execute(testcases)
