"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from sqlalchemy import *

from test_backend.server import db
# 第一个参数为中间表表名
# 其他参数分别为关联表的id，
testcase_plan_rel = db.Table(
    'testcase_plan_rel',
    # 如果中间表添加了id（非外键）。那么就需要设置自增，否则会出现为空的错误
    # 方法二： 中间表，不用添加主键id
    Column('id', Integer,
           primary_key=True,
           autoincrement=True
           ),
    Column('testcase_id', Integer,
           ForeignKey('testcase.id'),
           primary_key=True),
    Column('plan_id', Integer,
           ForeignKey('plan.id'),
           primary_key=True)
)