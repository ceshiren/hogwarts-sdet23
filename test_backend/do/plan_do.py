"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from sqlalchemy import *
from sqlalchemy.orm import relationship

from test_backend.do import testcase_plan_rel
from test_backend.server import db


class PlanDo(db.Model):
    # 表名
    __tablename__ = "plan"
    # 测试计划的id
    id = db.Column(Integer, primary_key=True)
    # 测试计划的名称
    name = db.Column(String(255), nullable=False, unique=True)
    # 关联的测试用例
    #  第一个参数：变量名为关联表的名称
    # 第二个参数： 中间表
    # 第三个参数选填： 反射
    # plan实例.testcases
    # testcase实例.plans
    testcases = relationship("TestcaseDo",
                             secondary=testcase_plan_rel,
                             backref="plans")
    def as_dict(self):
        return {"id": self.id, "name": self.name,
                "testcases": [testcase.uid for testcase in self.testcases]}
