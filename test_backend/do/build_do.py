"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from datetime import datetime

from sqlalchemy import *

from test_backend.server import db


class BuildDo(db.Model):
    # 表名
    __tablename__ = "build"
    # 用例ID 用例的唯 一标识
    id = db.Column(Integer, primary_key=True)
    # 指向plan表的外键, ForeignKey("关联表.id")
    plan_id = db.Column(Integer, ForeignKey("plan.id"))
    # 备注
    report = db.Column(String(255))
    # default 表示默认值，创建时间一般都为自动生成的字段。
    created_at = Column(DATETIME, default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    def as_dict(self):
        return {"id": self.id,
                "plan_id": self.plan_id,
                "report":self.report,
                "created_at": str(self.created_at)}