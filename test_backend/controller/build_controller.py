"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""

from flask import request
from flask_restx import Namespace, Resource

from test_backend.service.build_service import BuildService

build_ns = Namespace("build", description="构建记录管理啊")
build_service = BuildService()

@build_ns.route("")
class BuildController(Resource):

    get_parser = build_ns.parser()
    get_parser.add_argument("plan_id",type=int, location="args")
    @build_ns.expect(get_parser)
    def get(self):
        plan_id = request.args.get("plan_id")
        if plan_id:
            build_list = build_service.get_all_by_plan_id(plan_id)
            datas = [build.as_dict() for build in build_list]
            return {"code": 0 , "msg": {"data": datas}}
        else:
            return {"code": 40001, "msg": "plan id doesn't exist"}
