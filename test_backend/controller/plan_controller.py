"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""

from flask import request
from flask_restx import Namespace, Resource, fields

from test_backend.do.plan_do import PlanDo
from test_backend.service.plan_service import PlanService

plan_ns = Namespace("plan", description="测试计划管理")
plan_service = PlanService()

# 用作swagger 接口文档的说明
plan_model = plan_ns.model('Plan', {
    'name': fields.String,
    # 如果使用复杂的数据类型，需要添加内部使用的类型，否则就会报错
    'testcase_ids': fields.List(fields.Integer)
})

@plan_ns.route("")
class PlanController(Resource):

    def get(self):
        # 把python 对象转换为标准的dict(flask=>json)
        plan_list = plan_service.get_all()
        datas = [plan.as_dict() for plan in plan_list]
        return {"code": 0 , "msg": {"data": datas}}

    @plan_ns.expect(plan_model)
    def post(self):
        testcase_ids = request.json.get("testcase_ids")
        name = request.json.get("name")
        plan_entity = PlanDo(name=name)
        # 两种方式实现 测试计划的名称
        # 方式一： 由接口传递name给plan
        plan_service.save(testcase_ids, plan_entity)
        return {"code": 0 , "msg": "plan execute success"}
