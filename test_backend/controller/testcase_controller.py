"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from flask import request
from flask_restx import Namespace, Resource, fields

from test_backend.do import TestcaseDo
from test_backend.service.testcase_service import TestcaseService

testcase_ns = Namespace("case", description="用例管理")
testcase_service = TestcaseService()



@testcase_ns.route("")
class TestcaseController(Resource):
    get_parser = testcase_ns.parser()
    get_parser.add_argument("id", type=int, location="args")
    @testcase_ns.expect(get_parser)
    def get(self):
        testcase_id = request.args.get("id")
        if testcase_id:
            datas = [testcase_service.get(testcase_id).as_dict()]
        else:
            testcase_entity_list = testcase_service.get_all()
            datas = [testcase.as_dict() for testcase in testcase_entity_list]
        return {"code": 0, "msg": {"data": datas}}

    # 用作swagger 接口文档的说明
    testcase_model = testcase_ns.model('Testcase',
                           {'uid': fields.String,  # 如果使用复杂的数据类型，需要添加内部使用的类型，否则就会报错
                                    'remark': fields.String, })

    @testcase_ns.expect(testcase_model)
    def post(self):
        testcase = TestcaseDo(**request.json)
        testcase_service.save(testcase)
        return {"code": 0, "msg": "testcase add success"}

    delete_parser = testcase_ns.parser()
    delete_parser.add_argument("id", type=int, location="args")

    @testcase_ns.expect(get_parser)
    def delete(self):
        testcase_id = request.args.get("id")
        if testcase_id:
            testcase_service.delete(testcase_id)
            return {"code": 0, "msg": "testcase delete success"}
        else:
            return {"code": 0, "msg": "testcase delete failed"}


    @testcase_ns.expect(testcase_model)
    def put(self):
        testcase_service.update(request.json)
        return {"code": 0, "msg": "testcase update success"}
