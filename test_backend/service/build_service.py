"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from test_backend.dao.build_dao import BuildDao
from test_backend.do.build_do import BuildDo
from test_backend.service.testcase_service import TestcaseService

build_dao = BuildDao()
testcase_service = TestcaseService()

class BuildService:

    def get_all_by_plan_id(self, plan_id):
        return build_dao.get_all_by_plan_id(plan_id)

    # save 方法不会对controller暴漏。
    def save(self, build_entity:BuildDo):
        build_dao.save(build_entity)

