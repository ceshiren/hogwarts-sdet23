"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from typing import List

from test_backend.dao.plan_dao import PlanDao
from test_backend.do import TestcaseDo
from test_backend.do.plan_do import PlanDo
from test_backend.service.build_service import BuildService
from test_backend.service.testcase_service import TestcaseService
from test_backend.utils.jenkins_utils import JenkinsUtils
from test_backend.utils.log_util import logger

testcase_service = TestcaseService()
build_service = BuildService()
plan_dao = PlanDao()

class PlanService:
    """
    1. 生成测试计划
    2. 查看所有测试计划的功能
    """
    def save(self, testcase_id_list: List[int],plan_entity: PlanDo):
        """

        :param testcase_id_list: 测试用例的列表
        :param plan_entity: 测试计划的实体类
        :return:
        """
        # 测试计划具体关联了哪些用例信息
        # 错误写法 plan_entity.testcases = testcase_id_list
        # plan_entity.testcases = List[TestcaseDo]
        testcase_obj_list = [testcase_service.get(testcase_id)
                             for testcase_id in testcase_id_list]
        plan_entity.testcases = testcase_obj_list
        # 2. 执行测试用例，需要传入用例信息
        report = self.execute(testcase_obj_list)
        # 3. 测试计划写入数据
        plan_id = plan_dao.save(plan_entity)
        # 4. 构建记录写入数据
        build_entity = BuildDo(plan_id=plan_id, report=report)
        build_service.save(build_entity)


    def get_all(self):
        return plan_dao.get_all()

    # todo: 前端未实现
    def execute(self, testcases:List[TestcaseDo])-> str:
        """
        测试用例的执行
        :param testcases:
        :return: 返回执行的测试报告
        """
        testcase_uid_lsit = [testcase.uid for testcase in testcases]
        testcase_info = " ".join(testcase_uid_lsit)
        logger.info(f"要执行的测试用例为{testcase_info}")
        report = JenkinsUtils.invoke(testcase_info)
        logger.info(f"测试用例报告为{report}")

        return report