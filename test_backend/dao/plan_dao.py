"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from typing import List

from test_backend.do.plan_do import PlanDo
from test_backend.server import db_session


class PlanDao:

    def save(self, plan_entity: PlanDo):
        db_session.add(plan_entity)
        db_session.commit()
        # 如果想要获取写入数据库的id，需要在commit 之后 从实例获取到对应的id
        return plan_entity.id

    def get_all(self) -> List[PlanDo]:
        return db_session.query(PlanDo).all()
