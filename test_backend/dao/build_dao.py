"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from typing import List

from test_backend.do.build_do import BuildDo
from test_backend.server import db_session


class BuildDao:
    # 不再单独的获取所有的构建记录，而是根据plan_id去获取
    def get_all_by_plan_id(self, plan_id)-> List[BuildDo]:
        return db_session.query(BuildDo).filter_by(plan_id=plan_id).all()

    def save(self, build_entity:BuildDo):
        db_session.add(build_entity)
        db_session.commit()

    # def excute(self):