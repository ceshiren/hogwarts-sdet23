"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from test_backend.do.testcase_do import TestcaseDo
from test_backend.server import db_session


class TestcaseDao:
    def save(self, testcase_entity:TestcaseDo):
        db_session.add(testcase_entity)
        db_session.commit()

    def delete(self,testcase_id):
        db_session.query(TestcaseDo).filter_by(id=testcase_id).delete()
        db_session.commit()


    def get(self, testcase_id)->TestcaseDo:
        # db_session
        # TestcaseDao.query
        return db_session.query(TestcaseDo).filter_by(id=testcase_id).first()

    def get_all(self):
        return db_session.query(TestcaseDo).all()

    def update(self, testcase_id, update_testcase_data):
        db_session.query(TestcaseDo).filter_by(id=testcase_id).update(update_testcase_data)
        db_session.commit()
