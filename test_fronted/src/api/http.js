// 完成 http 请求的基本配置
// 导入 axios
import axios from 'axios'

// 创建 axios 实例
var instance = axios.create({
  // 请求头
  headers: {
    'Content-Type': 'application/json'
  },
  // 超时时间
  timeout: 2500,
  // 基础 url，接口服务地址
  // baseURL: 'http://39.102.48.202:6099/'
  baseURL: 'http://127.0.0.1:5000/'
})

export default instance