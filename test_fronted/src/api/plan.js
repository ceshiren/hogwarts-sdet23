// 测试用例增删改查接口管理

// 导入已经配置好的 axios 实例
import axios from './http'

const plan = {
  // 获取用例信息
  get(params){
    return axios({
      method: "GET",
      url: "/plan",
      // 如果是传递拼接在 url 中的参数，要使用 params
      params: params
    })
  },
  // 添加用例
  add(data) {
    return axios({
      method: "POST",
      url: "/plan",
      // 如果是传递请求体，要使用 data
      data: data
    })
  },
}

// 导出
export default plan