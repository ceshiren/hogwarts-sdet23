import axios from './http'

const build = {
  // 获取用例信息
  get(params){
    return axios({
      method: "GET",
      url: "/build",
      // 如果是传递拼接在 url 中的参数，要使用 params
      params: params
    })
  },
}

// 导出
export default build